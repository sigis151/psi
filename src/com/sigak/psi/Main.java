package com.sigak.psi;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		for (int i = 0; i <= 100; i++) {
			String a[] = { "-v", "5000", "-c", String.valueOf(i * 10), "-f", "data" + i };
			GeneratorMain.main(a);
			try {
				long time = System.currentTimeMillis();
				Process p = Runtime.getRuntime().exec(
						"DelegateLauncher.exe " + "-read " + "data" + i + " -saveGraph " + "datagraph" + i + " -saveTree " + "datatree" + i);
				p.waitFor();
				System.out.println(i*10 + " " + (System.currentTimeMillis() - time));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.exit(0);
	}

}
