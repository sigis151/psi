package com.sigak.psi;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

public class UI {
	private static final JFrame frame = new JFrame();
	private static final JProgressBar bar = new JProgressBar();
	public static int value;
	public static String task;

	static public void init(final List l) {
		frame.setSize(400, 50);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocation(500, 500);
		frame.add(bar);
		frame.setVisible(true);

	}

	static public void setMax(int max) {
		bar.setMaximum(max);
	}

	static public void start() {
		final Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				while (bar.getMaximum() >= value) {
					bar.setValue(value);
					frame.setTitle(task + " " + value + "/" + bar.getMaximum());
					if (bar.getMaximum() == value)
						break;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		});
		t.start();
	}
}
