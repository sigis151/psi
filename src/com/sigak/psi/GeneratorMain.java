package com.sigak.psi;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
	private static final String FILE_NAME = "data.txt";
	private static final int DEFAULT_CONNECTIONS_COUNT = 1;
	private static final int DEFAULT_VERTEX_COUNT = 5000;
	static public final Random ran = new Random();

	public static void main(String[] args) {
		int v = -1;
		int c = -1;
		try {
			for (int i = 0; i < args.length; i++) {
				switch (args[i]) {
				case "-v":
					v = Integer.valueOf(args[i + 1]);
					break;
				case "-c":
					c = Integer.valueOf(args[i + 1]);
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("error in params");
			System.exit(0);
		}
		final int vertexCount;
		final int connectionCount;
		if (v != -1) {
			vertexCount = v;
		} else
			vertexCount = DEFAULT_VERTEX_COUNT;
		if (c != -1) {
			connectionCount = c;
		} else
			connectionCount = DEFAULT_CONNECTIONS_COUNT;
		System.out.println("heap size:" + java.lang.Runtime.getRuntime().maxMemory());
		long minheap = 2048 * 1024;
		minheap *= 1024;
		if (java.lang.Runtime.getRuntime().maxMemory() < minheap) {
			System.out.println("heap to small(minimum " + minheap + "), finishing");
			System.exit(0);
		}
		if (vertexCount < 2) {
			System.out.println("to small vertex count");
			System.exit(0);
		}
		if (connectionCount <= 0 || connectionCount >= vertexCount) {
			System.out.println("incorect connection count");
			System.exit(0);
		}
		final List<Vertex> vertexes = new ArrayList<Vertex>();
		UI.init(vertexes);
		generateVertexPosition(vertexes, vertexCount);
		generateVertexConnection(vertexes, connectionCount);
		try {
			writeToFile(vertexes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			String[] cmd = { "WPF.exe" };
			Process p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(0);
	}

	private static void writeToFile(List<Vertex> vertexes) throws IOException {
		File f = new File(FILE_NAME);
		System.out.println(f.getAbsolutePath() + " " + f.delete());

		RandomAccessFile file = new RandomAccessFile(FILE_NAME, "rw");
		FileChannel channel = file.getChannel();
		ByteBuffer b = ByteBuffer.allocate(4);
		b.order(ByteOrder.LITTLE_ENDIAN);
		UI.value = 0;
		UI.task = "write";
		UI.start();
		// Collections.shuffle(vertexes);
		for (Vertex v : vertexes) {
			UI.value++;
			b.clear();
			b.putInt(v.getId());
			b.flip();
			channel.write(b);
			b.clear();
			b.putInt(v.getX());
			b.flip();
			channel.write(b);
			b.clear();
			b.putInt(v.getY());
			b.flip();
			channel.write(b);
			for (int c = 0; c < v.getConnections().size(); c++) {
				b.clear();
				b.putInt(v.getConnections().get(c));
				b.flip();
				channel.write(b);
				b.clear();
				b.putInt(v.getConnectionsWeight().get(c));
				b.flip();
				channel.write(b);
			}
			b.clear();
			b.putInt(0);
			b.flip();
			channel.write(b);
		}
		b.clear();
		b.putInt(0);
		b.flip();
		channel.write(b);
		channel.close();
		file.close();
	}

	private static void generateVertexConnection(List<Vertex> vertexes, int connectionCount) {
		UI.value = 0;
		UI.task = "connections";
		UI.start();
		int connection;
		int conns;
		int weight;
		for (int i = 1; i <= vertexes.size(); i++) {
			Vertex v = vertexes.get(i - 1);
			conns = ran.nextInt(connectionCount) + 1;// tasko jungciu kiekis
			while (v.getConnections().size() <= conns) {
				connection = ran.nextInt(vertexes.size()) + 1;
				while (connection == i || v.getConnections().contains(connection)) {
					connection = ran.nextInt(vertexes.size()) + 1;
				}
				weight = ran.nextInt(Integer.MAX_VALUE) + 1;
				v.getConnections().add(connection);
				v.getConnectionsWeight().add(weight);
				vertexes.get(connection - 1).getConnections().add(i);
				vertexes.get(connection - 1).getConnectionsWeight().add(weight);
			}
			UI.value = i;
		}

	}

	private static void generateVertexPosition(final List<Vertex> vertexes, int max) {
		UI.setMax(max);
		UI.value = 0;
		UI.task = "generate";
		UI.start();
		for (int i = 1; i <= max; i++) {
			Vertex add = new Vertex(i);
			boolean validcoords = false;
			while (!validcoords) {
				validcoords = true;
				for (Vertex v : vertexes) {
					if (v.coordsIsEqual(add.getX(), add.getY())) {
						add.randomizeCoords();
						validcoords = false;
						break;
					}
				}
			}
			UI.value = i + 1;
			vertexes.add(add);
		}
	}

}
