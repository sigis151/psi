package com.sigak.psi;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
	static final public int MAX_COORDINATE = 100;

	private int X;
	private int Y;
	private int id;

	Vertex(int id) {
		conn = new ArrayList<Integer>();
		connWeight = new ArrayList<Integer>();
		randomizeCoords();
		this.id = id;
	}

	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}

	public List<Integer> getConnections() {
		return conn;
	}

	public void setConnections(List<Integer> conn) {
		this.conn = conn;
	}

	List<Integer> conn;
	List<Integer> connWeight;

	public List<Integer> getConnectionsWeight() {
		return connWeight;
	}

	public void setConnectionsWeight(List<Integer> connWeight) {
		this.connWeight = connWeight;
	}

	public void randomizeCoords() {
		X = Main.ran.nextInt(MAX_COORDINATE) + 1;
		Y = Main.ran.nextInt(MAX_COORDINATE) + 1;
	}

	public boolean coordsIsEqual(int x, int y) {
		return X == x && Y == y;
	}

}